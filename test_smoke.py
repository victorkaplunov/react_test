""" Smoke test for example React TO DO application."""


def test_open_start_page(open_base_page):
    """Base page is open."""
    assert open_base_page.title == 'React • TodoMVC'


def test_create_first_to_do(add_todo, open_base_page):
    """Create new 'to do' and check presence."""
    assert open_base_page.find_elements_by_xpath("//*[contains(text(), 'first to_do')]")



