import pytest
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as con
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys


@pytest.fixture(scope="session")
def browser():
    # browser = webdriver.Firefox() # for local run
    browser = webdriver.Remote(
        command_executor='http://selenium__standalone-chrome:4444/wd/hub',
        desired_capabilities=DesiredCapabilities.CHROME)
    return browser


@pytest.fixture(scope="module")
def base_url():
    """Base URL"""
    base_url = "http://todomvc.com/examples/react/"
    return base_url


@pytest.fixture()
def open_base_page(browser, base_url):
    """Open React TO DO application."""
    browser.get(base_url)
    return browser


@pytest.fixture()
def add_todo(open_base_page, browser):
    """Wait field for to_do and create new to_do"""
    try:
        # Wait as long as required, or maximum of 10 sec for alert to appear
        WebDriverWait(open_base_page, 10).until(
            con.presence_of_element_located((By.CLASS_NAME, 'new-todo'))
        )
    except (ElementClickInterceptedException, TimeoutException) as py_ex:
        print(py_ex)
        print(py_ex.args)

    input_field = open_base_page.find_element_by_class_name('new-todo')
    input_field.send_keys('first to_do')
    input_field.send_keys(Keys.ENTER)
    browser.save_screenshot('screenshot.png')
    return input_field
